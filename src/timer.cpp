#include "timer.h"

#include "utils/util.h"

namespace first_try {

bool Timer::Comparator::operator()(const Timer::ptr& lhs
                                    ,const Timer::ptr& rhs) const {
    if (!lhs && !rhs) {
        return false;
    }
    if (!lhs) {
        return true;
    }
    if (!rhs) {
        return false; 
    }
    //左边时间小就返回true
    if (lhs->m_next < rhs->m_next) {
        return true;
    }
    if (lhs->m_next > rhs->m_next) {
        return false; 
    }
    return lhs.get() < rhs.get();
}

Timer::Timer(uint64_t ms, std::function<void ()> cb,
                bool recurring, TimerManager* manager)
                :m_recurring(recurring), m_ms(ms), m_cb(cb),
                m_manager(manager){
    m_next = utils::GetCurrentMs() + m_ms;
}

Timer::Timer(uint64_t next):
    m_next(next) {
}

bool Timer::cancel() {
    TimerManager::RWMutexType::WriteLock lock(m_manager->m_mutex);
    if (m_cb) {
        m_cb = nullptr;
        auto it = m_manager->m_timers.find(shared_from_this());
        m_manager->m_timers.erase(it);
        return true;
    }
    return false;
}

//重新设置时间
bool Timer::refresh() {
    TimerManager::RWMutexType::WriteLock lock(m_manager->m_mutex);
    if (!m_cb) {
        return false;
    }
    auto it = m_manager->m_timers.find(shared_from_this());
    if (it == m_manager->m_timers.end()) {
        return false;
    }
    m_manager->m_timers.erase(it);
    m_next = utils::GetCurrentMs() + m_ms;
    m_manager->m_timers.insert(shared_from_this());
    return true;
}

//重置间隔时间， formNow:从何时开始生效
bool Timer::reset(uint64_t ms, bool fromNow) {
    if (ms == m_ms && !fromNow) {
        return true;
    }
    TimerManager::RWMutexType::WriteLock lock(m_manager->m_mutex);
    if (!m_cb) {
        return false;
    }
    auto it = m_manager->m_timers.find(shared_from_this());
    if (it == m_manager->m_timers.end()) {
        return false;
    }
    m_manager->m_timers.erase(it);
    uint64_t start = 0;
    if (fromNow) {
        start = utils::GetCurrentMs();
    } else {
        start = m_next - m_ms;
    }
    m_ms = ms;
    m_next = start + m_ms;
    m_manager->addTimer(shared_from_this(), lock);
    return true;
}

TimerManager::TimerManager() {
    m_previousTime = utils::GetCurrentMs();
}

TimerManager::~TimerManager() {

}

//增加一个定时器
Timer::ptr TimerManager::addTimer(uint64_t ms, std::function<void()> cb, bool recurring) {
    Timer::ptr timer(new Timer(ms, cb, recurring, this));
    RWMutexType::WriteLock lock(m_mutex);
    addTimer(timer, lock);
    return timer;
}

static void OnTimer(std::weak_ptr<void> weakCond, std::function<void()> cb) {
    std::shared_ptr<void> tmp = weakCond.lock();
    if (tmp) {
        cb(); 
    }
}


//增加一个条件定时器//条件存在的时候才继续触发
Timer::ptr TimerManager::addConditionTimer(uint64_t ms, std::function<void()> cb, std::weak_ptr<void> weakCond,
                                bool recurring) {
    return addTimer(ms, std::bind(&OnTimer, weakCond, cb), recurring);
}

uint64_t TimerManager::getNextTimer() {
    RWMutexType::ReadLock lock(m_mutex);
    m_tickled = false;
    if (m_timers.empty()) {
        return ~0ull;
    }

    const Timer::ptr& next = *m_timers.begin();
    uint64_t nowMs = utils::GetCurrentMs();
    if (nowMs >= next->m_next) {
        return 0;
    } else {
        return next->m_next - nowMs;
    }
}

void TimerManager::listExpiredCb(std::vector<std::function<void()>>& cbs) {
    uint64_t nowMs = utils::GetCurrentMs();
    std::vector<Timer::ptr> expired;
    {
        RWMutexType::ReadLock lock(m_mutex);
        if (m_timers.empty()) {
            return;
        }
    }
    RWMutexType::WriteLock lock(m_mutex);
    // 没有修改时间不会超时
    bool rollover = deleteClockRollover(nowMs);
    if (!rollover && ((*m_timers.begin())->m_next > nowMs)) {
        return;
    }

    Timer::ptr nowTimer(new Timer(nowMs));
    auto it = rollover ? m_timers.end() : m_timers.lower_bound(nowTimer);
    while(it != m_timers.end() && (*it)->m_next == nowMs) {
        ++it;
    }
    expired.insert(expired.begin(), m_timers.begin(), it);
    m_timers.erase(m_timers.begin(), it);
    cbs.reserve(expired.size());

    for (auto& timer : expired) {
        cbs.push_back(timer->m_cb);
        if(timer->m_recurring) {
            timer->m_next = nowMs + timer->m_ms;
            m_timers.insert(timer);
        } else {
            timer->m_cb = nullptr;
        }
    
    }
}

void TimerManager::addTimer(Timer::ptr timer, RWMutexType::WriteLock& lock) {
    auto it = m_timers.insert(timer).first;
    bool at_front = (it == m_timers.begin()) && !m_tickled;
    if (at_front) {
        m_tickled = true;
    }

    lock.unlock();
    if (at_front) {
        onTimerInsertedAtFront();
    }
}

bool TimerManager::deleteClockRollover(uint64_t nowMs) {
    bool rollover = false;
    if (nowMs < m_previousTime && 
            nowMs < (m_previousTime - 60*60 * 1000)) {
        rollover = true;            
    }
    m_previousTime = nowMs;
    return rollover;
}


bool TimerManager::hasTimer() {
    RWMutexType::ReadLock lock(m_mutex);
    return m_timers.empty();
}


}
