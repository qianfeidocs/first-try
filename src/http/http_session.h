#ifndef HTTP_SESSION_H
#define HTTP_SESSION_H

#include "../socket_stream.h"
#include "http.h"

namespace first_try {
namespace http{
class HttpSession  : public SocketStream {
	public:
		typedef std::shared_ptr<HttpSession> ptr;
		HttpSession(Socket::ptr sock, bool owner = true);

		HttpRequest::ptr recvRequest();
		int sendRespone(HttpResponse::ptr rsp);

};

}}
#endif