#ifndef __FT_NONCOPYABLE_H__
#define __FT_NONCOPYABLE_H__

namespace first_try {

class Noncopyable {
public:
    Noncopyable() = default;
    ~Noncopyable() = default;
    Noncopyable(const Noncopyable&) = delete;
    Noncopyable& operator=(const Noncopyable&) = delete;
};

}

#endif

