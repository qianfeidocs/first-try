#include "test_socket.h"

#include "log.h"
#include "iomanager.h"
#include "socket.h"
#include "utils/util.h"

namespace first_try {

static first_try::Logger::ptr g_logger = FT_LOG_NAME("t_skt");

void test_socket() {
    first_try::IPAddress::ptr addr = first_try::Address::LookupAnyIPAddress("www.baidu.com");
    if (addr) {
        FT_LOG_INFO(g_logger)<<"get address :"<< addr->toString();
    } else {
        FT_LOG_ERROR(g_logger)<<"get address fail";
        return ;
    }
    first_try::Socket::ptr sock = first_try::Socket::CreateTCP(addr);
    addr->setPort(80);
    FT_LOG_DEBUG(g_logger)<<addr->toString();
    if (!sock->connect(addr)) {
        FT_LOG_ERROR(g_logger)<<"connect "<< addr->toString() <<" fail";
        return;
    } else {
        FT_LOG_INFO(g_logger) <<"connect "<< addr->toString() << " connected";
    }

    const char buff[] = "GET / HTTP/1.0\r\n\r\n";
    int rt = sock->send(buff, sizeof(buff));
    if(rt <= 0) {
        FT_LOG_INFO(g_logger) << "send fail rt=" << rt;
        return;
    }
    std::string buffs;
    buffs.resize(4096);
    rt = sock->recv(&buffs[0], buffs.size());

    if(rt <= 0) {
        FT_LOG_INFO(g_logger) << "recv fail rt=" << rt;
        return;
    }

    buffs.resize(rt);
    FT_LOG_INFO(g_logger) << buffs;
}

void TestSocket::testSocket() {
    first_try::IOManager iom;
    iom.schedule(&test_socket);
}


}
