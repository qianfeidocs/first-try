#ifndef __FT_ENDIAN_H__
#define __FT_ENDIAN_H__

#define FT_LITTLE_ENDIAN 1
#define FT_BIG_ENDIAN 2

#include <byteswap.h>
#include <stdint.h>

namespace first_try {

template<class T>
typename std::enable_if<sizeof(T) == sizeof(uint64_t), T>::type
byteswap(T value) {
    return (T)bswap_64((uint64_t)value);
}

template<class T>
typename std::enable_if<sizeof(T) == sizeof(uint32_t), T>::type
byteswap(T value) {
    return (T)bswap_32((uint32_t)value);
}

template<class T>
typename std::enable_if<sizeof(T) == sizeof(uint16_t), T>::type
byteswap(T value) {
    return (T)bswap_16((uint16_t)value);
}

#if BYTE_ORDER == BIG_ENDIAN
#define FT_BYTE_ORDER FT_BIG_ENDIAN
#else
#define FT_BYTE_ORDER FT_LITTLE_ENDIAN
#endif

#if FT_BYTE_ORDER == FT_BIG_ENDIAN
template<class T>
T byteswapOnLittleEndian(T t) {
    return t;
}

template<class T>
T byteswapOnBigEndian(T t) {
    return byteswap(t);
}

#else

template<class T>
T byteswapOnLittleEndian(T t) {
    return byteswap(t);
}

template<class T>
T byteswapOnBigEndian(T t) {
    return t;
}

#endif

}

#endif
