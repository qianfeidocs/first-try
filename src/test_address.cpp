#include "test_address.h" 

#include "address.h"
#include "log.h"
#include "utils/util.h"

namespace first_try{

static first_try::Logger::ptr g_logger = FT_LOG_NAME("t_adrs");

static void test() {
    std::vector<Address::ptr> addrs;

    FT_LOG_INFO(g_logger) << " begin()";
    bool v = Address::Lookup(addrs, "www.baidu.com");
    FT_LOG_INFO(g_logger) << " end()";
    if (!v) {
        FT_LOG_ERROR(g_logger) <<"lookup fail";
        return;
    }

    for (size_t i = 0; i < addrs.size(); ++i) {
        FT_LOG_INFO(g_logger)<<" i = "<<i<<" - "<< addrs[i]->toString();
    }
}

void test_iface() {
    std::multimap<std::string, std::pair<Address::ptr, uint32_t> > results;
    bool v = Address::GetInterfaceAddresses(results);
    if (!v) {
        FT_LOG_ERROR(g_logger) <<" GetInterfaceAddress ";
        return;
    }

    for (auto& i: results) {
        FT_LOG_INFO(g_logger)<<" i = "<<i.first<<" - "<< i.second.first->toString() <<" - "<< i.second.second;
    }
}

void TestAddress::testAddress() {
    test();
    test_iface();
}

}
