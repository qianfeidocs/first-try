#ifndef __FIRST_TRY_LOG_H__
#define __FIRST_TRY_LOG_H__

#include <fstream>
#include <map>
#include <memory>
#include <list>
#include <stdarg.h>
#include <stdint.h>
#include <sstream>
#include <string>
#include <vector>

#include "thread.h"
#include "utils/singleton.h"
#include "utils/util.h"

//末尾不要加分号会隔断stream
#define FIRST_TRY_LEVEL(logger, level) \
    if(logger->getLevel()<=level) \
        first_try::LogEventWrap(first_try::LogEvent::ptr(new first_try::LogEvent(logger, level, __FILE__, __LINE__, 0, utils::GetThreadId(), utils::GetFiberId(), time(0), first_try::Thread::GetName()))).getSS()

#define FIRST_TRY_LOG_DEBUG(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::DEBUG)
#define FIRST_TRY_LOG_INFO(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::INFO)
#define FIRST_TRY_LOG_WARN(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::WARN)
#define FIRST_TRY_LOG_ERROR(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::ERROR)
#define FIRST_TRY_LOG_FATAL(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::FATAL)

#define FT_LOG_DEBUG(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::DEBUG)
#define FT_LOG_INFO(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::INFO)
#define FT_LOG_WARN(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::WARN)
#define FT_LOG_ERROR(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::ERROR)
#define FT_LOG_FATAL(logger) FIRST_TRY_LEVEL(logger, first_try::LogLevel::FATAL)

#define FIRST_TRY_FMT_LEVEL(logger, level, fmt, ...) \
    if (logger->getLevel() <= level) \
        first_try::LogEventWrap(first_try::LogEvent::ptr(new first_try::LogEvent(logger, level, __FILE__, __LINE__, 0, utils::GetThreadId(), utils::GetFiberId(),time(0), first_try::Thread::GetName()))).getEvent()->format(fmt, __VA_ARGS__)

#define FIRST_TRY_FMT_LOG_DEBUG(logger, fmt, ...) FIRST_TRY_FMT_LEVEL(logger, first_try::LogLevel::DEBUG, fmt, __VA_ARGS__)
#define FIRST_TRY_FMT_LOG_INFO(logger, fmt, ...) FIRST_TRY_FMT_LEVEL(logger, first_try::LogLevel::INFO, fmt, __VA_ARGS__)
#define FIRST_TRY_FMT_LOG_WARN(logger, fmt, ...) FIRST_TRY_FMT_LEVEL(logger, first_try::LogLevel::WARN, fmt, __VA_ARGS__)
#define FIRST_TRY_FMT_LOG_ERROR(logger, fmt, ...) FIRST_TRY_FMT_LEVEL(logger, first_try::LogLevel::ERROR, fmt, __VA_ARGS__)
#define FIRST_TRY_FMT_LOG_FATAL(logger, fmt, ...) FIRST_TRY_FMT_LEVEL(logger, first_try::LogLevel::FATAL, fmt, __VA_ARGS__)

#define FIRST_TRY_LOG_ROOT() first_try::LoggerMgr::GetInstance()->getRoot()
#define FT_LOG_ROOT() first_try::LoggerMgr::GetInstance()->getRoot()
#define FT_LOG_NAME(name) first_try::LoggerMgr::GetInstance()->getLogger(name)
#define FIRST_TRY_LOG_NAME(name) first_try::LoggerMgr::GetInstance()->getLogger(name)


namespace first_try {

class Logger;
class LoggerManager;

//日志级别
class LogLevel {
public:
    enum Level {
        UNKNOW = 0,
        DEBUG = 1,
        INFO = 2,
        WARN = 3,
        ERROR = 4,
        FATAL = 5
    };

    static const char* ToString(LogLevel::Level level);
    static LogLevel::Level FromString(const std::string& str);
};

//日志事件
class LogEvent {
public:
    typedef std::shared_ptr<LogEvent> ptr;
    LogEvent(std::shared_ptr<Logger> logger, LogLevel::Level level
            ,const char* file, int32_t m_line, uint32_t elapse
            ,uint32_t thread_id, uint32_t fiber_id, uint64_t time
            ,const std::string& thread_name);

    const char* getFile() const { return m_file;}
    int32_t getLine() const { return m_line;}
    uint32_t getElapse() const { return m_elapse;}
    uint32_t getThreadId() const { return m_threadId;}
    uint32_t getFiberId() const { return m_fiberId;}
    uint64_t getTime() const { return m_time;}
    const std::string& getThreadName() const { return m_threadName;}
    std::string getContent() const { return m_ss.str();}
    std::shared_ptr<Logger> getLogger() const { return m_logger;}
    LogLevel::Level getLevel() const { return m_level;}

    std::stringstream& getSS() { return m_ss;}
    void format(const char* fmt, ...);
    void format(const char* fmt, va_list al);
private:
    const char* m_file = nullptr;  //文件名
    int32_t m_line = 0;            //行号
    uint32_t m_elapse = 0;         //程序启动开始到现在的毫秒数
    uint32_t m_threadId = 0;       //线程id
    uint32_t m_fiberId = 0;        //协程id
    uint64_t m_time = 0;           //时间戳
    std::string m_threadName;
    std::stringstream m_ss;

    std::shared_ptr<Logger> m_logger;
    LogLevel::Level m_level;
};

class LogEventWrap {
public:
    LogEventWrap(LogEvent::ptr e);
    ~LogEventWrap();
    LogEvent::ptr getEvent() const { return m_event;}
    std::stringstream& getSS();
private:
    LogEvent::ptr m_event;
};

//日志格式器
class LogFormatter {
public:
    typedef std::shared_ptr<LogFormatter> ptr;
    LogFormatter(const std::string& pattern);

    //%t    %thread_id %m%n
    std::string format(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event);
public:
    class FormatItem {
    public:
        typedef std::shared_ptr<FormatItem> ptr;
        virtual ~FormatItem() {}
        virtual void format(std::ostream& os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) = 0;
    };

    void init();

    bool isError() const { return m_error;}
    const std::string getPattern() const { return m_pattern;}
private:
    std::string m_pattern;
    std::vector<FormatItem::ptr> m_items;
    bool m_error = false;

};

//日志输出地
class LogAppender {
friend class Logger;
public:
    typedef std::shared_ptr<LogAppender> ptr;
    typedef Spinlock MutexType;
    virtual ~LogAppender() {}

    virtual void log(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) = 0;

    void setFormatter(LogFormatter::ptr val);
    LogFormatter::ptr getFormatter();

    LogLevel::Level getLevel() const { return m_level;}
    void setLevel(LogLevel::Level val) { m_level = val;}
protected:
    LogLevel::Level m_level = LogLevel::DEBUG;
    bool m_hasFormatter = false;
    MutexType m_mutex;
    LogFormatter::ptr m_formatter;
};

//日志器
class Logger : public std::enable_shared_from_this<Logger> {
friend class LoggerManager;
public:
    typedef std::shared_ptr<Logger> ptr;
    typedef Spinlock MutexType;

    Logger(const std::string& name = "root");
    void log(LogLevel::Level level, LogEvent::ptr event);

    void debug(LogEvent::ptr event);
    void info(LogEvent::ptr event);
    void warn(LogEvent::ptr event);
    void error(LogEvent::ptr event);
    void fatal(LogEvent::ptr event);

    void addAppender(LogAppender::ptr appender);
    void delAppender(LogAppender::ptr appender);
    void clearAppenders();
    LogLevel::Level getLevel() const { return m_level;}
    void setLevel(LogLevel::Level val) { m_level = val;}

    const std::string& getName() const { return m_name;}

    void setFormatter(LogFormatter::ptr val);
    void setFormatter(const std::string& val);
    LogFormatter::ptr getFormatter();

private:
    std::string m_name;                     //日志名称
    LogLevel::Level m_level;                //日志级别
    MutexType m_mutex;
    std::list<LogAppender::ptr> m_appenders;//Appender集合
    LogFormatter::ptr m_formatter;
    Logger::ptr m_root;
};

//输出到控制台的Appender
class StdoutLogAppender : public LogAppender {
public:
    typedef std::shared_ptr<StdoutLogAppender> ptr;
    void log(Logger::ptr logger, LogLevel::Level level, LogEvent::ptr event) override;
};

//定义输出到文件的Appender
class FileLogAppender : public LogAppender {
public:
    typedef std::shared_ptr<FileLogAppender> ptr;
    FileLogAppender(const std::string& filename);
    void log(Logger::ptr logger, LogLevel::Level level, LogEvent::ptr event) override;

    //重新打开文件，文件打开成功返回true
    bool reopen();
private:
    std::string m_filename;
    std::ofstream m_filestream;
    uint64_t m_lastTime = 0;
};

class LoggerManager {
public:
    typedef Spinlock MutexType;
    LoggerManager();
    Logger::ptr getLogger(const std::string& name);

    void init();
    Logger::ptr getRoot() const { return m_root;}

private:
    MutexType m_mutex;
    std::map<std::string, Logger::ptr> m_loggers;
    Logger::ptr m_root;
};

typedef first_try::Singleton<LoggerManager> LoggerMgr;

}

#endif
