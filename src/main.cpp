#include <iostream>


#include "config.h"
#include "log.h"

#include "test_include.h"

using namespace std;
using namespace first_try;

static first_try::Logger::ptr g_log = FT_LOG_NAME("t_main");

int main() {

    //测试日记
    //TestLog::testLog();

    //测试线程
    //TestThread::testThread();

    //打印层次信息
    //FT_LOG_INFO(g_log) << utils::BacktraceToString(2,0, "    ");

    //测试简单调度
    //TestScheduler::testSchedulerSimple();

    //测试io
    //TestIOManager::testIOManager();

    //测试定时器
    //TestTimer::testTimer();

    //测试地址
    //TestAddress::testAddress();

    //测试socket
    //TestSocket::testSocket();

    //TestByteArray::testByteArray();

    //TestHttp::testHttp();

    //测试tcpserver
    TestTcpServer::testTcpServer();

    FT_LOG_INFO(g_log)  << "Hello first try";
    return 0;
}
























