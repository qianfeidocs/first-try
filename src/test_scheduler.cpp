#include "test_scheduler.h"

#include "log.h"
#include "thread.h" 
#include "scheduler.h"
#include "utils/util.h"

namespace first_try {

static first_try::Logger::ptr g_logger = FT_LOG_NAME("t_sch");


void test_fiber() {
    static int s_count = 20;
    FT_LOG_INFO(g_logger) << " test_fiber scount="<< s_count;
    if (--s_count > 0) {
        Scheduler::GetThis()->schedule(&test_fiber);
    }
}

void TestScheduler::testSchedulerSimple() {
    Scheduler sc(10, true, "test");
    sc.start();
    sc.schedule(&test_fiber);
    //FT_LOG_INFO(g_logger) << " test_fiber";
    sc.stop();
}


}
