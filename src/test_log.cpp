#include "test_log.h"

#include <boost/lexical_cast.hpp>
#include <boost/make_shared.hpp>

#include "config.h"
#include "log.h"

namespace first_try {

ConfigVar<int>::ptr testValConfig = Config::Lookup("test.value", (int)1125, "test value");
static first_try::Logger::ptr g_log = FT_LOG_NAME("main");

void TestLog::testLog() {

    Logger::ptr logger(new Logger);

    logger->addAppender(LogAppender::ptr(new StdoutLogAppender));

    LogEvent::ptr event(new LogEvent(logger, LogLevel::DEBUG, __FILE__, __LINE__, 0, utils::GetThreadId(), utils::GetFiberId(), time(0), "测试日记"));
    event->getSS()<<"Hello";
    logger->log(LogLevel::DEBUG, event);

    FIRST_TRY_LOG_DEBUG(logger) <<" Hello";

    int a = 10;
    FIRST_TRY_FMT_LOG_DEBUG(logger, " sss %s ", "ss");

    FIRST_TRY_LOG_DEBUG(logger) <<" Hello "<< a;

    auto lg = LoggerMgr::GetInstance()->getLogger("xx");
    FIRST_TRY_LOG_INFO(lg) << " name : xxx";

    FIRST_TRY_LOG_INFO(lg) << testValConfig->getValue();
}

}

