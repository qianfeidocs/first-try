#ifndef __FIRST_TRY_MACRO_H__
#define __FIRST_TRY_MACRO_H__

#include <string.h>
#include <assert.h>
#include "utils/util.h"

//https://www.jianshu.com/p/2684613a300f
#if defined __GNUC__ || defined __llvm__
#   define FT_LICKLY(x)       __builtin_expect(!!(x), 1)
#   define FT_UNLICKLY(x)     __builtin_expect(!!(x), 0)
#else
#   define FT_LICKLY(x)      (x)
#   define FT_UNLICKLY(x)      (x)
#endif

#define FIRST_TRY_ASSERT(x) \
    if(FT_UNLICKLY(!(x))) { \
        FIRST_TRY_LOG_ERROR(FIRST_TRY_LOG_ROOT()) << "ASSERTION: " #x \
            << "\nbacktrace:\n" \
            << first_try::utils::BacktraceToString(100, 2, "    "); \
        assert(x); \
    }

#define FT_ASSERT(x) FIRST_TRY_ASSERT(x)

#define FIRST_TRY_ASSERT2(x, w) \
    if(FT_UNLICKLY(!(x))) { \
        FIRST_TRY_LOG_ERROR(FIRST_TRY_LOG_ROOT()) << "ASSERTION: " #x \
            << "\n" << w \
            << "\nbacktrace:\n" \
            << first_try::utils::BacktraceToString(100, 2, "    "); \
        assert(x); \
    }

#define FT_ASSERT2(x, w) FIRST_TRY_ASSERT2(x, w)
#endif
