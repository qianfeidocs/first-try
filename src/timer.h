#pragma once

#include <memory>
#include <vector>
#include <set>

#include "thread.h"

namespace first_try {

class TimerManager;

class Timer : public  std::enable_shared_from_this<Timer> {
friend class TimerManager;
public:
    typedef std::shared_ptr<Timer> ptr;
    bool cancel();
    bool refresh();
    //重置间隔时间， formNow:从何时开始生效
    bool reset(uint64_t ms, bool fromNow);
private:
    Timer(uint64_t ms, std::function<void()> cb,
            bool recurring, TimerManager* manager);
    Timer(uint64_t next);
private: 
    //是否循环定时器
    bool m_recurring = false;
    //执行周期
    uint64_t m_ms = 0;
    //精准的执行时间
    uint64_t m_next = 0;
    std::function<void()> m_cb;
    TimerManager* m_manager= nullptr;
private:
    //排序比较
    struct Comparator{
        bool operator()(const Timer::ptr& lhs, const Timer::ptr& rhs) const; 
    };
};


class TimerManager {
friend class Timer;
public:
    typedef RWMutex RWMutexType;

    TimerManager();
    virtual ~TimerManager();

    //增加一个定时器
    Timer::ptr addTimer(uint64_t ms, std::function<void()> cb, bool recurring = false);

    //增加一个条件定时器//条件存在的时候才继续触发
    Timer::ptr addConditionTimer(uint64_t ms, std::function<void()> cb, std::weak_ptr<void> weakCond, bool recurring = false);

    uint64_t getNextTimer();

    void listExpiredCb(std::vector<std::function<void()> >& cbs);

    bool hasTimer();
protected:
    //插入到最前端
    virtual void onTimerInsertedAtFront() = 0;

    void addTimer(Timer::ptr timer, RWMutexType::WriteLock& lock);
private:
    bool deleteClockRollover(uint64_t nowMs);

private:
    RWMutexType m_mutex;
    std::set<Timer::ptr, Timer::Comparator> m_timers;
    bool m_tickled = false;
    uint64_t m_previousTime = 0;
};

}



