#include  "test_tcp_server.h"

#include "tcp_server.h"
#include "log.h"
#include "iomanager.h"

namespace first_try {

static first_try::Logger::ptr g_logger = FT_LOG_NAME("t_tsr");

static void run(){
    auto addr = first_try::Address::LookupAny("127.0.0.1:80");
    std::vector<first_try::Address::ptr> addrs,fails;
    addrs.push_back(addr);

    first_try::TcpServer::ptr tcp_server(new first_try::TcpServer);
    while(!tcp_server->bind(addrs, fails)) {
        sleep(2);
    }
    tcp_server->start();
}

void TestTcpServer::testTcpServer() {
    IOManager iom(2);
    iom.schedule(run);
}


}