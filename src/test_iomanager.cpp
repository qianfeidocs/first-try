#include "test_iomanager.h" 

#include <assert.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "iomanager.h"
#include "log.h"
#include "utils/util.h"

namespace first_try{

static first_try::Logger::ptr g_logger = FT_LOG_NAME("t_iom");
int sock = 0;

void testFiber() {
    FT_LOG_INFO(g_logger) << " test _ iomanager test fiber";
    sock = socket(AF_INET, SOCK_STREAM, 0);
    fcntl(sock, F_SETFL, O_NONBLOCK);

    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(80);
    //ping www.baidu.com address
    inet_pton(AF_INET, "112.80.248.75", &addr.sin_addr.s_addr);

    if(!connect(sock, (const sockaddr*)&addr, sizeof(addr))) {

    } else if (errno ==EINPROGRESS) {
        IOManager::GetThis()->addEvent(sock, IOManager::READ, [](){
            FT_LOG_INFO(g_logger)<< "read callback";
            });
        IOManager::GetThis()->addEvent(sock, IOManager::WRITE, [](){
            FT_LOG_INFO(g_logger)<< "write callback";
            IOManager::GetThis()->cancelEvent(sock, IOManager::READ);
            close(sock);
            });
    } else {
        FT_LOG_ERROR(g_logger) << " else" << errno <<" " <<strerror(errno);
    }
}

void test() {
    IOManager iom;
    iom.schedule(&testFiber);
}

void TestIOManager::testIOManager() {
    test();
}

}
