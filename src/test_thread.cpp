#include "test_thread.h"

#include <boost/make_shared.hpp>

//#include "config.h"
#include "log.h"
#include "thread.h" 
#include "utils/util.h"

namespace first_try {

static first_try::Logger::ptr g_log = FT_LOG_NAME("test_thread");

static int cnt = 0;
//first_try::RWMutex s_mutex;
first_try::Mutex s_mutex;
void fun1() {
    //ps -aux | grep xxxx
    //top -H -p pid 查看线程
    FT_LOG_INFO(g_log)  << " name: " <<Thread::GetName()<< " this.name: " <<Thread::GetThis()->getName()<< " id: "<< utils::GetThreadId() << " this.id: "<< Thread::GetThis()->getId();
    for (int i =0;i<1000000;i++) {
       //first_try::RWMutex::WriteLock lock(s_mutex);
       first_try::Mutex::Lock lock(s_mutex);
       ++cnt;
    }
}

void TestThread::testThread() {
    std::vector<Thread::ptr> thrs;
    for (int i = 0; i < 20; i++) {
         Thread::ptr thr(new Thread(&fun1, "name_"+std::to_string(i))); 
         thrs.push_back(thr);
    }
    for(int i = 0; i< 20 ; ++i) {
        thrs[i]->join(); 
    }
    FT_LOG_INFO(g_log) << " count" << cnt;
}


}
