#ifndef __FIRST_TRY_UTIL_H__
#define __FIRST_TRY_UTIL_H__

#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <vector>
#include <stdio.h>
#include <stdint.h>
#include <string>

namespace first_try {
namespace utils{
//获取线程Id 
pid_t GetThreadId();

uint32_t GetFiberId();

//层信息
void Backtrace(std::vector<std::string>& bt, int size = 64, int skip = 1);

std::string BacktraceToString(int size = 64, int skip = 2, const std::string& prefix = "");


uint64_t GetCurrentMs();
uint64_t GetCurrentUs();

}
}

#endif
