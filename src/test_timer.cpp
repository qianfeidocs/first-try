#include "test_timer.h" 

#include <assert.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "iomanager.h"
#include "log.h"
#include "utils/util.h"

namespace first_try{

static first_try::Logger::ptr g_logger = FT_LOG_NAME("t_tmr");

static void test() {
    FT_LOG_INFO(g_logger)<<" timer start";
    IOManager iom(2);
    iom.addTimer(500, []() {
        FT_LOG_INFO(g_logger)<<" hello timer";
    }, false);
    FT_LOG_INFO(g_logger)<<" timer end";
}

void TestTimer::testTimer() {
    test();
}

}
