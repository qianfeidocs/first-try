#include "test_http.h"

#include "http/http.h"

namespace first_try {

void testRequest() {
    first_try::http::HttpRequest::ptr req(new first_try::http::HttpRequest);
    req->setHeader("host" , "www.sylar.top");
    req->setBody("hello sylar");
    req->dump(std::cout) << std::endl;

}

void testRespone(){
  first_try::http::HttpResponse::ptr rsp(new first_try::http::HttpResponse);
  rsp->setHeader("X-X", "sylar");
  rsp->setBody("hello sylar");
  rsp->setStatus((first_try::http::HttpStatus)400);
  rsp->setClose(false);

  rsp->dump(std::cout) << std::endl;
}


void TestHttp::testHttp() {
    testRequest();
    testRespone();
}

}